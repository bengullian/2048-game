﻿using _2048_Game.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace _2048_Game.Helpers
{
    public class _2048Game : BoardGame, I_2048Game
    {
        private I_2048GameHorizontalMove horizontalMove;
        private I_2048GameVerticalMove verticalMove;

        public int score = 0;
        public string message;
        

        public _2048Game(int size, I_2048GameHorizontalMove hMove, I_2048GameVerticalMove vMove) : base(size)
        {
            horizontalMove = hMove;
            verticalMove = vMove;
            InitializeBoard();
        }


        public int[][] getEmptyGrid(int size)
        {
            return base.CreateEmptyGrid(size);
        }


        public void InitializeBoard()
        {            
            this.Grid = InsertNumberInGrid(GetRandomPlayNumber(), this.Grid);           

        }


        public int GetRandomPlayNumber()
        {
            double number = new Random().NextDouble();
            return number >= 0.5 ? 4 : 2;
        }


        private int[] GetColumnArray(int columnNumber, int[][] grid)
        {
            int[] column = new int[4];
            int pointer = 0;
            while (pointer < grid.Length)
            {
                column[pointer] = grid[pointer][columnNumber];
                pointer++;
            }
            return column;
        }


        private int[][] FitRowInGrid(int[][] grid, int rowNo, int[] row, string direction)
        {
            if (direction == "right")
            {
                Array.Copy(row, 0, grid[rowNo], grid.Length - row.Length, row.Length);
            }

            else if (direction == "left")
            {
                Array.Copy(row, 0, grid[rowNo], 0, row.Length);
            }
            return grid;
        }


        private int[][] FitColumnInGrid(int[][] grid, int columnNo, int[] column, string direction)
        {
            if (direction == "up")
            {
                for (int x = 0; x < column.Length; x++)
                {
                    grid[x][columnNo] = column[x];
                }
            }

            else if (direction == "down")
            {
                int pointer = grid.Length - 1;      //since its a square grid, length == breadth
                for (int x = column.Length - 1; x >= 0; x--)
                {
                    grid[pointer][columnNo] = column[x]; // we start to fill from the end
                    pointer--;
                }
            }

            return grid;
        }



        public int[] CombineCells(int[] array)
        {
            for (int x = 0; x <= array.Length - 2; x++)
            {
                if (array[x] == array[x + 1])
                {
                    this.score += array[x] + array[x + 1];
                    array[x] += array[x + 1];
                    array[x + 1] = 0;
                }
            }

            int[] values = Array.FindAll(array, y => !y.Equals(0)).ToArray();

            return values;
        }


        public int[][] HandleArrowKeyPress(int[][] grid, string direction)
        {
            int[][] tempGid = grid;
            switch (direction)
            {
                case "ArrowUp":
                    tempGid = this.verticalMove.MoveSlides(this, "up");
                    break;

                case "ArrowDown":
                    tempGid = this.verticalMove.MoveSlides(this, "down");
                    break;

                case "ArrowRight":
                    tempGid = this.horizontalMove.MoveSlides(this, "right");
                    break;

                case "ArrowLeft":
                    tempGid = this.horizontalMove.MoveSlides(this, "left");
                    break;
            }

            return tempGid;
        }

        //both arrays are of equal size, we can compare them by looping through 
        public bool hasGameStateChanged(int[][] grid)
        {
            for (int x = 0; x < grid.Length; x++)
            {
                for (int y = 0; y < grid.Length; y++)
                {
                    if (grid[x][y] != this.Grid[x][y])
                    {
                        return true;
                    }
                }
            }
            return false;
        }
    }
}
