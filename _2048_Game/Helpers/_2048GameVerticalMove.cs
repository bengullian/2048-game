﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using _2048_Game.Models;

namespace _2048_Game.Helpers
{
    public class _2048GameVerticalMove : I_2048GameVerticalMove
    {
        
        public int[][] MoveSlides(_2048Game gameBoard, string direction)
        {
            int[][] newGrid = gameBoard.getEmptyGrid(gameBoard.Grid.Length);
            for (int x = 0; x < gameBoard.Grid.Length; x++)
            {
                // get the column data we are interested in 
                int[] tmp = this.GetColumnArray(x, gameBoard.Grid);
                
                // remove zeros by returning a new array with values greater than zero
                int[] values = Array.FindAll(tmp, y => !y.Equals(0)).ToArray();

                // add adjacent cells with like values
                if (values.Length > 0)
                {
                    tmp = gameBoard.CombineCells(values);
                    // calling it twice ansures all like adjacent cells are added up
                    tmp = gameBoard.CombineCells(tmp);
                }

                //fit the column back into the grid with the new values
                newGrid = FitColumnInGrid(newGrid, x, tmp, direction);
            }

            return newGrid;
        }


        public int[][] FitColumnInGrid(int[][] grid, int columnNo, int[] column, string direction)
        {
            // here we reverse the process of GetColumnArray by inserting the sum values from above back into the grid
            if (direction == "up")
            {
                for (int x = 0; x < column.Length; x++)
                {
                    grid[x][columnNo] = column[x];
                }
            }

            else if (direction == "down")
            {
                int pointer = grid.Length - 1;      //since its a square grid, length == breadth
                for (int x = column.Length - 1; x >= 0; x--)
                {
                    grid[pointer][columnNo] = column[x]; // we start to fill from the end
                    pointer--;
                }
            }

            return grid;
        }


        public int[] GetColumnArray(int columnNumber, int[][] grid)
        {
            int[] column = new int[4];
            int pointer = 0;
            while (pointer < grid.Length)
            {
                column[pointer] = grid[pointer][columnNumber];
                pointer++;
            }
            return column;
        }


    }
}
