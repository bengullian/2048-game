﻿namespace _2048_Game.Helpers
{
    public interface I_2048GameVerticalMove
    {
        int[][] FitColumnInGrid(int[][] grid, int columnNo, int[] column, string direction);
        int[] GetColumnArray(int columnNumber, int[][] grid);
        int[][] MoveSlides(_2048Game gameBoard, string direction);
    }
}