﻿namespace _2048_Game.Helpers
{
    public interface I_2048Game
    {
        int[] CombineCells(int[] array);
        int GetRandomPlayNumber();
        void InitializeBoard();
        int[][] getEmptyGrid(int size);
        int[][] HandleArrowKeyPress(int[][] grid, string direction);
        bool hasGameStateChanged(int[][] grid);
    }
}