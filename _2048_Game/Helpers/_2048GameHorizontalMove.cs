﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using _2048_Game.Models;

namespace _2048_Game.Helpers 
{
    public class _2048GameHorizontalMove : I_2048GameHorizontalMove
    {              

        public int[][] MoveSlides(_2048Game gameBoard, string direction)
        {
            int[][] grid = gameBoard.Grid;
            int[][] newGrid = gameBoard.CreateEmptyGrid(grid.Length);

            for (int x = 0; x < grid.Length; x++)
            {
                int[] tmp = grid[x];

                //remove zeros(empty cells) from the array by return only values greater than zero
                int[] values = Array.FindAll(tmp, y => !y.Equals(0)).ToArray();

                if (values.Length > 0)
                {
                    // add adjacent cells with similar values
                    tmp = gameBoard.CombineCells(values);
                    // call it again to ensure all like adjacent cells are added
                    tmp = gameBoard.CombineCells(tmp);
                }

                // fit row back into the grid
                newGrid = FitRowInGrid(newGrid, x, tmp, direction);
            }

            return newGrid;
        }


        public int[][] FitRowInGrid(int[][] grid, int rowNo, int[] row, string direction)
        {
            if (direction == "right")
            {
                Array.Copy(row, 0, grid[rowNo], grid.Length - row.Length, row.Length);
            }

            else if (direction == "left")
            {
                Array.Copy(row, 0, grid[rowNo], 0, row.Length);
            }
            return grid;
        }
       
    }
}
