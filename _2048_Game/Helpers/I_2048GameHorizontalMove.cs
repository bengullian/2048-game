﻿using _2048_Game.Models;

namespace _2048_Game.Helpers
{
    public interface I_2048GameHorizontalMove
    {
        int[][] FitRowInGrid(int[][] grid, int rowNo, int[] row, string direction);
        int[][] MoveSlides(_2048Game gameBoard, string direction);
    }
}