﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using _2048_Game.Helpers;
using _2048_Game.Models;
using Microsoft.AspNetCore.Mvc;

namespace _2048_Game.Controllers
{
    public class GameBoardController : Controller
    {
        private static _2048Game gameGrid;
       

        public IActionResult GameBoard()
        {
           
            gameGrid = new _2048Game(4, new _2048GameHorizontalMove(), new _2048GameVerticalMove());
            return View(gameGrid);
        }

        [HttpGet]
        public ActionResult Played(string key)
        {
            int[][] grid = gameGrid.HandleArrowKeyPress(gameGrid.Grid, key);

            // only update the dom and insert a new random number if the previous state has changed
            if (gameGrid.hasGameStateChanged(grid))
            {                  
                gameGrid.Grid = gameGrid.InsertNumberInGrid(gameGrid.GetRandomPlayNumber(), grid);
            }

            ViewBag.Score = gameGrid.score;
          
            return PartialView("Grid", gameGrid);
        }
    }
}