﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.
var page;

$(document).ready(function () {    
    page = document.getElementById('body');
    page.addEventListener('keydown', (e) => {

        if (e.key.includes('Arrow')) {
            let request = new XMLHttpRequest();            
            request.open('GET', '/Gameboard/Played/' + e.key);
            request.setRequestHeader('Content-Type', 'application/json');
            request.addEventListener('load', reload);
            request.send();
        }      
    });    
});


function reload() {
    $("#grid").html(this.responseText);
}


