﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace _2048_Game.Models
{
    public class BoardGame : IBoardGame
    {
        private int gridSize;

        private int[][] gameGrid;


        public BoardGame(int size)
        {
            this.gridSize = size;
            this.gameGrid = this.CreateEmptyGrid(gridSize);
        }


        public int[][] Grid
        {
            get
            {
                return gameGrid;
            }
            set
            {
                gameGrid = value;
            }
        }


        public int GridSize()
        {
            return gridSize;
        }
               

        public int[][] CreateEmptyGrid(int size)
        {
            int[][] grid = new int[size][];

            for (int x = 0; x < size; x++)
            {
                grid[x] = new int[4];
            }
            return grid;
        }


        public int[][] InsertNumberInGrid(int number, int[][] grid)
        {
            int numberPosition = new Random().Next(16);

            while (grid[(int)(numberPosition / 4)][numberPosition % 4] != 0)
            {
                numberPosition = new Random().Next(16);
            }

            grid[(int)(numberPosition / 4)][numberPosition % 4] = number;

            return grid;
        }




    }
}
