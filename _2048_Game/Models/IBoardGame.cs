﻿namespace _2048_Game.Models
{
    public interface IBoardGame
    {        
        int GridSize();
        int[][] CreateEmptyGrid(int size);
        int[][] InsertNumberInGrid(int number, int[][] grid);
    }
}